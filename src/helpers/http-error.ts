import type HttpStatusCode from '@/types/http-status-code';

/**
 * Creates a new HttpError
 * @class
 */
export default class HttpError extends Error {
    public status: number = 500;

    /**
     * Constructor Options
     * @param {string} message - error message
     * @param {HttpStatusCode|undefined} status - http status code
     */
    constructor(message: string, status?: HttpStatusCode) {
        super(message);

        this.status = status || 500;
    }
}
