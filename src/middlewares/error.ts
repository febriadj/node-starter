import { NextFunction as Next, Request, Response } from 'express';
import HttpError from '@/helpers/http-error';

export default async (
    err: HttpError,
    req: Request,
    res: Response,
    next: Next
): Promise<void> => {
    res.status(err.status || 500).json({
        success: false,
        message: err.message,
    });

    next();
};
