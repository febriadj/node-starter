import dotenv from 'dotenv';

dotenv.config();

import express, { Application } from 'express';
import morgan from 'morgan';
import error from '@/middlewares/error';

const app: Application = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(morgan('tiny'));

app.get('/', (req, res) => {
    res.status(200).json('hello, world!');
});

app.use(error);

export default app;
